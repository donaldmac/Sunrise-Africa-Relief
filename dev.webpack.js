const path = require( 'path' );
const BrowserSyncPlugin = require( 'browser-sync-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const OptimizeCSSAssetsPlugin = require( 'optimize-css-assets-webpack-plugin' );
const CopyPlugin = require( 'copy-webpack-plugin' );

module.exports = {
	mode: 'development',
	devtool: 'source-map',
	entry: './web/app/themes/sunriseafrica/src/js/index.js',
	output: {
		path: path.resolve( __dirname, './web/app/themes/sunriseafrica/dist' ),
		filename: 'bundle.js',
		publicPath: './dist/',
	},

	plugins: [
		new MiniCssExtractPlugin( { filename: '../style.css' } ),
		// Browser-sync for local development
		new BrowserSyncPlugin( {
			files: [
				'**/*.php',
				'**/src/**/*',
				'**/templates/**/*',
				'tailwind.config.js',
			],
			injectChanges: true,
			proxy: 'http://sunriseafrica.test',
			injectCss: true,
			notify: false,
			open: false,
		} ),

		// Minify CSS assets
		new OptimizeCSSAssetsPlugin( {} ),
		// Copy images to directory images directory within 'dist'
		new CopyPlugin( [
			{
				from: 'web/app/themes/sunriseafrica/src/images/*.*',
				to: 'images/',
				flatten: true,
			},
			// Copy all the JS files from admin-dashboard directory to js directory in dist.
			{
				from: 'web/app/themes/sunriseafrica/src/js/admin_dashboard/*.*',
				to: 'js/',
				flatten: true,
			},
		] ),
	],

	module: {
		rules: [
			{
				// Extract any SCSS content and minimize
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					{
						loader: 'postcss-loader',
					},
					{
						loader: 'sass-loader',
						options: {},
					},
				],
			},
			{
				// Extract any CSS content and minimize
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					{ loader: 'postcss-loader' },
				],
			},
		],
	},
};
