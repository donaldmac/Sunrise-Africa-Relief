<?php
/**
 * Sunrise Africa Relief theme
 * https://github.com/donald-m/Sunrise-Africa-Relief
 *
 * @package  WordPress
 * @subpackage  SunriseAfricaRelief Theme
 * @since   SunriseAfricaRelief Theme 0.1
 */

/**
 * Class Sunrise_Africa
 * For theme configuration, including posts types and features of WordPress, plus customizations to the WordPress Theme Customizer.
 */
class Sunrise_Africa extends Timber\Site {

	/** Add timber support and other functions to be inserted into WordPress loop. */
	public function __construct() {
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'rename_default_post_type_to_news' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'customize_register', array( $this, 'theme_customize_register' ) );
		add_action( 'wp_head', array( $this, 'theme_insert_customized_css' ) );
		add_action( 'customize_preview_init', array( $this, 'add_theme_customizer_live_preview' ) );
		add_action( 'login_enqueue_scripts', array( $this, 'customize_login_logo' ) );
		parent::__construct();
	}

	/** Register custom post types. */
	public function register_post_types() {

		// Register specific Blog Post custom post type.
		$labels = array(
			'name'               => 'Blog Post',
			'singular'           => 'Blog Post',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New',
			'edit_item'          => 'Edit Blog Post',
			'new_item'           => 'New Blog Post',
			'all_items'          => 'All Blog Posts',
			'view_items'         => 'View Blog Posts',
			'search_items'       => 'Search Blog Posts',
			'not_found'          => 'No Blog Posts found',
			'not_found_in_trash' => 'No Blog Posts found in Trash',
			'menu_name'          => 'Blog Posts',
		);

		// Show 'Blog Posts' in WordPress admin dashboard, with icon from standard WP icons 'dashicons-analytics'.
		$args = array(
			'labels'        => $labels,
			'public'        => true,
			'has_archive'   => true,
			'rewrite'       => true,
			'supports'      => array( 'title', 'editor', 'author', 'comments', 'revisions', 'thumbnail' ),
			'menu_position' => 5,
			'menu_icon'     => 'dashicons-analytics',
			'show_in_rest'  => true,
		);

		register_post_type( 'blog', $args );
	}

	/** Rename default 'Post' type to 'News Post' because it makes sense alongside 'Blog Post' custom type. */
	public function rename_default_post_type_to_news() {
		$get_post_type              = get_post_type_object( 'post' );
		$labels                     = $get_post_type->labels;
		$labels->name               = 'News Post';
		$labels->singular_name      = 'News Post';
		$labels->add_new            = 'Add News Post';
		$labels->add_new_item       = 'Add News Post';
		$labels->edit_item          = 'Edit News Post';
		$labels->new_item           = 'News Post';
		$labels->view_item          = 'View News Posts';
		$labels->search_items       = 'Search News Posts';
		$labels->not_found          = 'No News Posts found';
		$labels->not_found_in_trash = 'No News Posts found in Trash';
		$labels->all_items          = 'All News Posts';
		$labels->menu_name          = 'News Posts';
		$labels->name_admin_bar     = 'News Posts';
	}


	/**
	 * Add features to WordPress installation.
	 *
	 * See https://developer.wordpress.org/reference/functions/add_theme_support/
	 */
	public function theme_supports() {
		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );

		/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		* Enable support for Post Formats.
		*
		* See: https://codex.wordpress.org/Post_Formats
		*/
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** This is where you add some context
	 *
	 * @param array $context context['this'] Being the Twig's {{ this }}.
	 *
	 * @return array
	 */
	public function add_to_context( $context ) {
		$context['menu'] = new Timber\Menu();
		$context['site'] = $this;

		return $context;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param object $twig get extension.
	 *
	 * @return string
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );

		return $twig;
	}

	/** Enable Customisation of theme settings in WordPress customizer
	 *
	 * @param object $wp_customize is the customization object.
	 */
	public function theme_customize_register( $wp_customize ) {

		// Change Blog Name And Blog Description to 'postMessage' in order for WP Customizer to use Javascript real-time refresh.
		$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

		// Add Text Colour.
		$wp_customize->add_setting(
			'text_color',
			array(
				'default'   => '#121212',
				'transport' => 'postMessage',
			)
		);

		// Add Footer Background Colour.
		$wp_customize->add_setting(
			'footer_color',
			array(
				'default'   => '#ebebeb',
				'transport' => 'postMessage',
			)
		);

		// Add Header Background Colour.
		$wp_customize->add_setting(
			'header_background_color',
			array(
				'default'   => '#fff',
				'transport' => 'postMessage',
			)
		);

		// Add Headings Colour.
		$wp_customize->add_setting(
			'headings_color',
			array(
				'default'   => '#121212',
				'transport' => 'postMessage',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'headings_color',
				array(
					'label'   => __( 'Headings Colour', 'theme' ),
					'section' => 'colors',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'text_color',
				array(
					'label'   => __( 'Text Colour', 'theme' ),
					'section' => 'colors',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'header_background_color',
				array(
					'label'   => __( 'Header Background Colour', 'theme' ),
					'section' => 'colors',
				)
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'footer_color',
				array(
					'label'   => __( 'Footer Background Colour', 'theme' ),
					'section' => 'colors',
				)
			)
		);
	}

	// TODO: Add font options to the customizer, consider including google fonts.

	/** Insert Variables from Customizer into the head of the page
	 */
	public function theme_insert_customized_css() {
		// phpcs:disable
		?>
        <style type="text/css">
            p {
                color: <?php echo get_theme_mod('text_color', '#121212'); ?>;
            }

            header {
                background-color: <?php echo get_theme_mod('header_background_color', '#fff')   ?>;
            }

            footer {
                background-color: <?php echo get_theme_mod('footer_background_color', '#fff')   ?> !important;
            }

            h1, h2, h3, h4, h5, h6 {
                color: <?php echo get_theme_mod('headings_color', '#121212'); ?>;
            }
        </style>
		<?php
		// phpcs:enable
	}

	/**
	 * Enqueue the JS file for Live Preview for Theme Customiser.
	 */
	public function add_theme_customizer_live_preview() {
		wp_enqueue_script(
			'themecustomizer',
			get_template_directory_uri() . '/dist/js/customizer.js',
			array( 'jquery', 'customize-preview' ),
			1.0,
			true
		);
	}

	/** Change the Login Logo to Sunrise Africa Relief
	 */
	public function customize_login_logo() {
		// phpcs:disable
		?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url(<?php echo get_template_directory_uri(); ?>/src/images/login_logo_com.png);
                height: 167px;
                width: 216px;
                background-size: 216px 167px;
                background-repeat: no-repeat;
                padding-bottom: 30px;
            }
        </style>
		<?php
	}
	// phpcs:enable

}
